CC=gcc
LUA=lua5.1

CFLAGS=-Wall -Wextra -pedantic -std=c11 -O2 -march=native
LUA_CFLAGS!=pkg-config --cflags $(LUA)
LUA_LIBS!=pkg-config --libs $(LUA)

TEST_EXE=t
TEST_SRC=$(TEST_EXE).c
TEST_OBJ=$(TEST_EXE).o
TEST_LUA=t.lua

LIB=utf8_string_width
LIB_SRC=$(LIB).c
LIB_HDR=$(LIB).h
LIB_OBJ=$(LIB).o
LIB_SHR=lib$(LIB).so


.PHONY: all test c_test lua_test clean


all: test

test: c_test lua_test

c_test: $(TEST_EXE)
	@-echo Running C tests...
	@./$(TEST_EXE)

lua_test: $(TEST_LUA) $(LIB_SHR)
	@-echo Running Lua tests...
	@-$(LUA) $(TEST_LUA)


$(TEST_EXE): $(TEST_OBJ) $(LIB_OBJ)
	$(CC) $(CFLAGS) $^ -o $@ $(LUA_LIBS)

$(LIB_OBJ): $(LIB_SRC)
	$(CC) $(CFLAGS) $(LUA_CFLAGS) $< -c

$(LIB_SHR): $(LIB_SRC)
	$(CC) $(CFLAGS) $(LUA_CFLAGS) $< -shared -fPIC -o $@

$(TEST_OBJ): $(LIB_HDR)
$(LIB_OBJ): $(LIB_HDR)
$(LIB_SHR): $(LIB_HDR)


clean:
	@-rm -f $(TEST_EXE) *.o *.so

