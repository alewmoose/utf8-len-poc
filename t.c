#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include <assert.h>
#include "utf8_string_width.h"

int run_tests(void);

int main(void)
{
	/*
	 * NOTE: utf8 locale is required
	 * LC_CTYPE should work too
	 */
	if (!setlocale(LC_ALL, "en_US.UTF-8")) {
		perror("setlocale");
		exit(1);
	}

	return run_tests();
}


typedef struct {
	char   *string;
	size_t size;
	int    width_expected;
} Test;

int run_test(int test_no, Test t)
{
	int width_got = utf8_string_width(t.string, t.size);

	if (width_got == t.width_expected)
		return 0;

	fprintf(
		stderr,
		"Test #%d failed:\n"
		"\tstring         : \"%s\"\n"
		"\tsize           : %4lu\n"
		"\twidth_expected : %4d\n"
		"\twidth_got      : %4d\n",
		test_no, t.string, t.size, t.width_expected, width_got
	);
	return 1;
}

int run_tests(void)
{
	const int default_size = 256;
	char *weird_str = "เลขเด็ด 'แม่ผึ้ง พุ่มพวง' งวดนี้ต้องรวยให้ได้ คลิก!!";
	char no_null_str[] = "Тест";
	size_t nns_len = strlen(no_null_str);
	no_null_str[nns_len++] = '?';

	Test tests[] = {
		{ "", 0, 0 },
		{ "", default_size, 0 },

		{ "test", 0, 0 },
		{ "test", 1, 1 },
		{ "test", default_size, 4 },

		{ "тест", default_size, 4 },
		{ "тест", 8, 4 },
		{ "тест", 1, -1 },
		{ "тест", 2, 1 },

		{ weird_str, default_size, 40 },
		{ weird_str, strlen(weird_str), 40 },
		{ weird_str, 1, -1 },
		{ weird_str, 2, -1 },
		{ weird_str, 3, 1 },

		{ no_null_str, 0, 0 },
		{ no_null_str, nns_len, nns_len / 2 + 1  },
	};
	int nr_tests = (int)(sizeof(tests) / sizeof(Test));

	int failed = 0;
	for (int i = 0; i < nr_tests; i++) {
		failed += run_test(i+1, tests[i]);
	}

	if (failed > 0)
		return 2;

	fputs("OK\n", stderr);
	return 0;
}
