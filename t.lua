-- NOTE: utf8 locale is required
if not os.setlocale('en_US.UTF-8') then
    error("Failed to set locale")
end

local string_width = package.loadlib("./libutf8_string_width.so", "string_width")
if not string_width then
    error("Failed to load the lib")
end

assert(pcall(function () string_width() end) == false)
assert(pcall(string_width, nil) == false)
assert(pcall(string_width, {}) == false)

assert(string_width("") == 0)
assert(string_width("test") == 4)
assert(string_width("тест") == 4)
assert(string_width("เลขเด็ด 'แม่ผึ้ง พุ่มพวง' งวดนี้ต้องรวยให้ได้ คลิก!!") == 40)


-- NOTE:
-- There's a caveat: lua strings can contain the \0 character.
-- In that case, calculated length will equal to number of characters
-- before the \0:
assert(string_width("\0") == 0)
assert(string_width("ab\0cd") == 2)
assert(string_width("АБ\0ВГ") == 2)


print("OK")

