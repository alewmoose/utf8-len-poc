#define _GNU_SOURCE
#include <wchar.h>
#include <uchar.h>
#include <assert.h>
#include <lua.h>
#include "utf8_string_width.h"

#define MIN(a,b) ((a) < (b) ? (a) : (b))

/*
 * Alternative wcwidth implementation (have not testsed it):
 * https://www.cl.cam.ac.uk/~mgk25/ucs/wcwidth.c
 */

/*
 * See also:
 * http://www.cplusplus.com/reference/cuchar/mbrtoc32/
 */
int utf8_string_width(const char *restrict str, size_t size)
{
	assert(str != NULL);

	/*
	 * size is required when the string is not null-terminated.
	 * When the string *is* null-terminated, it still doesn't hurt -
	 * think of strncpy and alike
	 */

	mbstate_t state = {0};
	int width = 0;
	size_t bytes = 0;

	while (bytes < size) {
		char32_t c;
		size_t r = mbrtoc32(&c, str, MIN(size-bytes, 4), &state);
		if (r == (size_t)-1) {
			/* encoding error */
			return -1;
		}
		if (r == (size_t)-2) {
			/* multibyte char is larger than bytes read */
			return -1;
		}
		if (r == 0) {
			/* null character */
			break;
		}
		bytes += r;
		width += wcwidth(c);
		str += r;
	}

	return width;
}

int string_width(lua_State *L)
{
	if (lua_gettop(L) != 1 || !lua_isstring(L, 1)) {
		lua_pushstring(L, "Expected one argument of type \"string\"");
		lua_error(L);
	}

	size_t len;
	const char *str = lua_tolstring(L, 1, &len);

	if (!str) {
		lua_pushstring(L, "Unexpected error. This should not have happend :)");
		lua_error(L);
	}

	int width = utf8_string_width(str, len);
	lua_pushinteger(L, width);
	return 1;
}
