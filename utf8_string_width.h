#ifndef UTF8_STRING_WIDTH_H
#define UTF8_STRING_WIDTH_H

#include <stdlib.h>

/*
 * Count width of utf8 string
 * str: string, possibly null-terminated
 * size: length of the string
 * On error returns int < 0
 */
int utf8_string_width(const char *restrict str, size_t size);

#endif
